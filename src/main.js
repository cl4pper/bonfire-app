import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';

// ROUTES LINK
import Login from './routes/Login/Login.vue';
import Home from './routes/Home/Home.vue';
import Bag from './routes/Bag/Bag.vue';
import Party from './routes/Party/Party.vue';
import Guide from './routes/Guide/Guide.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Login },
  { path: '/home', component: Home },
  { path: '/bag', component: Bag },
  { path: '/party', component: Party },
  { path: '/guide', component: Guide },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
